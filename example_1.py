if False:
    a = 1 + "fff"
else:
    a = 1 + 2
print(a)
print(type(a))
a = "fff"
print(type(a))

# JAVA
# String thing;
# thing = "fff";

# print(bool(a))
# print(a.__bool__() or a.__len__())


class Hobbit:
    def __len__(self):
        return 100


class Orc:
    def __len__(self):
        return 300

a = Hobbit()
print(len(a))
