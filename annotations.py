import math


def circumference(radius: float) -> float:
    return 2 * math.pi * radius


pi: float = 3.141592


def circumference(radius: float) -> float:
    return 2 * pi * radius

def headline(
    text,           # type: str
    width=80,       # type: int
    fill_char="-",  # type: str
):                  # type: (...) -> str
    return f" {text.title()} ".center(width, fill_char)

print(headline("type comments work", width=40))