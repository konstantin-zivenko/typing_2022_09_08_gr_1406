# names: list = ["Ivan", "Semen"]
# version: tuple = (3, 10, 4)
# options: dict = {"centered": False, "capitalize": True}

from typing import Dict, List, Tuple, NoReturn

names: List[str] = ["Ivan", "Semen"]
version: Tuple[int, int, int] = (3, 10, 4)
options: Dict[str, bool] = {"centered": False, "capitalize": True}


def play(player_name: str) -> None:
    print(f"{player_name} plays")


def black_hole() -> NoReturn:
    raise Exception("ggggg")